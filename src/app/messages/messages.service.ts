import { Injectable } from '@angular/core';

@Injectable()
export class MessagesService {
  // All messages
  messages: string[] = [];

  constructor() { }

  /**
   * Adds a message
   * @param message Message
   */
  addMessage(...message: string[]): void {
    this.messages.push(message.join(': '));
  }

  // Clears all messages
  clear(): void {
    this.messages = [];
  }
}
