import { Component, OnInit } from '@angular/core';
import { HeroesService } from '../heroes.service';
import { Heroes } from '../mock-heroes';
import { Hero } from '../hero';

@Component({
  selector: 'app-hero-list',
  templateUrl: './hero-list.component.html',
  styleUrls: ['./hero-list.component.scss']
})
export class HeroListComponent implements OnInit {
  // All heroes
  heroes: Heroes;

  // Hero name filter
  heroNameFilter: string;

  constructor(private heroesService: HeroesService) { }

  ngOnInit() {
    this.fetchHeroes();
  }

  // Fetch all heroes
  fetchHeroes(): void {
    this.heroesService.getHeroes().subscribe(heroes => this.heroes = heroes);
  }

  /**
   * Delete hero by id
   * @param hero Hero id
   */
  onDelete(hero: Hero): void {
    this.heroesService.deleteHero(hero).subscribe(_ => this.fetchHeroes());
  }
}
