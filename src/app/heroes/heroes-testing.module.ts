import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { HeroesModule } from './heroes.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    RouterTestingModule,
    HeroesModule
  ],
  exports: [
    BrowserAnimationsModule,
    RouterTestingModule,
    HeroesModule
  ]
})
export class HeroesTestingModule { }
