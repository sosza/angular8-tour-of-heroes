import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroRegisterComponent } from './hero-register.component';
import { HeroesTestingModule } from '../heroes-testing.module';

describe('HeroRegisterComponent', () => {
  let component: HeroRegisterComponent;
  let fixture: ComponentFixture<HeroRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HeroesTestingModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
