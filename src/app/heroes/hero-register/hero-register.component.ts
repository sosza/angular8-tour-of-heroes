import { Component } from '@angular/core';
import { Hero } from '../hero';
import { HeroesService } from '../heroes.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-hero-register',
  templateUrl: './hero-register.component.html',
  styleUrls: ['./hero-register.component.scss']
})
export class HeroRegisterComponent {
  // New hero name
  heroName: string;

  constructor(
    private heroesService: HeroesService,
    private location: Location
  ) { }

  // Adds new hero if name was written
  onAdd(): void {
    if (this.heroName) {
      this.heroesService.addHero({name: this.heroName} as Hero).subscribe(_ => this.location.back());
    }
  }
}
