import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeroListComponent } from './hero-list/hero-list.component';
import { HeroDetailsComponent } from './hero-details/hero-details.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeroRegisterComponent } from './hero-register/hero-register.component';


const routes: Routes = [
  { path: 'heroes', component: HeroListComponent },
  { path: 'hero/register', component: HeroRegisterComponent },
  { path: 'hero/:id/details', component: HeroDetailsComponent },
  { path: 'dashboard', component: DashboardComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HeroesRoutingModule { }
