import { TestBed } from '@angular/core/testing';

import { HeroesService } from './heroes.service';
import { HeroesTestingModule } from './heroes-testing.module';

describe('HeroesService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HeroesTestingModule]
  }));

  it('should be created', () => {
    const service: HeroesService = TestBed.get(HeroesService);
    expect(service).toBeTruthy();
  });
});
