import { Injectable } from '@angular/core';
import { heroes, Heroes } from './mock-heroes';
import { Observable, of } from 'rxjs';
import { take, tap } from 'rxjs/operators';
import { Hero } from './hero';
import { MessagesService } from '../messages/messages.service';

@Injectable()
export class HeroesService {
  // All folks derived from const
  private heroes: Heroes = heroes;

  constructor(private messagesService: MessagesService) { }

  // Get all heroes
  getHeroes(): Observable<Heroes> {
    return of<Heroes>(this.heroes).pipe(
      take(1),
      tap(_ => this.addMessage('fetched all heroes'))
    );
  }

  /**
   * Get one hero by id
   * @param id Hero id
   */
  getHero(id: number): Observable<Hero> {
    return of<Hero>(this.heroes.find(hero => hero.id === id)).pipe(
      take(1),
      tap(_ => this.addMessage('fetched hero with id', id.toString()))
    );
  }

  /**
   * Adds new hero
   * @param hero New hero data
   */
  addHero(hero: Hero): Observable<Hero> {
    hero.id = Math.max(...this.heroes.map(currentHero => currentHero.id)) + 1;
    this.heroes.push(hero);
    return of<Hero>(hero).pipe(
      take(1),
      tap(newHero => this.addMessage(`added hero #${newHero.id} with name`, newHero.name))
    );
  }

  /**
   * Updates hero information
   * @param hero Hero data
   */
  updateHero(hero: Hero): Observable<Hero> {
    this.heroes = this.heroes.map(currentHero => currentHero.id === hero.id ? hero : currentHero);
    return of<Hero>(hero).pipe(
      take(1),
      tap(_ => this.addMessage('updated hero with id', hero.id.toString()))
    );
  }

  /**
   * Deletes hero by id
   * @param hero Hero id
   */
  deleteHero(hero: Hero): Observable<Hero> {
    this.heroes = this.heroes.filter(currentHero => currentHero.id !== hero.id);
    return of<Hero>(hero).pipe(
      take(1),
      tap(_ => this.addMessage('deleted hero with id', hero.id.toString()))
    );
  }

  /**
   * Normalizes messages added to messagesService
   * @param message Message
   */
  private addMessage(...message: string[]): void {
    this.messagesService.addMessage('HeroesService', ...message);
  }
}
